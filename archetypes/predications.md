---
title: "Prédication du {{ .Name | replaceRE "([0-9]{4})-([0-9]{2})-([0-9]{2})" "$3/$2/$1" }}"
date: {{ .File | replaceRE "-" "/" }}
creationDate: {{ .Date }}
author: Michel Block
---


