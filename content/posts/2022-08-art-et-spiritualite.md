---
title: "Arts et spiritualité"
---

Festival du 13 au 21 aout 2022.

Visualisez : 

- [l'affiche de l'événement](/202208_affiche1.pdf)
- [le programme complet](/202208_programme.pdf)
- [les intervenants](/202208_intervenants.pdf)

Les affiches pour chaque jour :

- [13 aout à 20h30](/202208_affiche2.pdf)
- [18 aout à 20h30](/202208_affiche3.pdf)
- [19 et 20 aout à 20h30](/202208_affiche4.pdf)
- [20 aout à 20h30](/202208_affiche5.pdf)

