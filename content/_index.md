## Bienvenue

... sur le site web de la paroisse protestante de Brest et du nord Finistère. Membre de l'Église protestante Unie de France.

### Nous contacter

36 rue Voltaire
29200 Brest

02 98 44 28 08

Notre [page de présentation](https://www.eglise-protestante-unie.fr/brest-et-nord-finistere-p50111) sur le site de l'Église Protestante Unie de France.

Pasteur Michel Block <pasteur@protestant-brest.fr>

### Horaires des cultes

Les culte ont lieu tous les dimanche à 10h30 au temple de Brest, 36 rue Voltaire.

À partir de septembre 2022, tous les dimanche soir à 18h30 [sur Skype](https://join.skype.com/iVqkiV3xScZV).


### Groupe de prière

Le groupe de prière se réunit tous les jours à 7h30, 12h00 et 18h30 [sur Skype](https://join.skype.com/d2KmrRgzSMgR).

### Chaine Youtube

Vous pouvez revoir les cultes et quelques vidéos sur notre [chaine Youtube](https://www.youtube.com/channel/UCoAqeQKq3MGPoYOJNjKAyvw)


### D'autres liens

  - Église Protestante Unie de France : https://www.eglise-protestante-unie.fr/
